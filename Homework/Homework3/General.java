import java.util.Arrays;
import java.util.Scanner;

class General {
	public static int arraySum = 0;
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int aLong = scanner.nextInt();
		int[] arrays = new int [aLong];
		System.out.println(ThinkArray(arrays));
		System.out.println(ExpandArray(arrays));
		System.out.println(FindtheMiddle(arrays));
		System.out.println(MinReverseMax(arrays));
		System.out.println(BubbleArray(arrays, aLong));
		System.out.println(NumbersArray());
	}
	public static int ThinkArray(int[] array) {
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
			arraySum = arraySum + array[i];
		}
		System.out.println("Exit one Array");
		return arraySum;
	}
	public static int ExpandArray(int[] array) {
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}
		for (int j = array.length - 1; j >= 0; j--){
			System.out.print(array[j] + " ");
		}
		System.out.println(Arrays.toString(array));
		System.out.println("Exit two array");
		return 0;
	}
	public static double FindtheMiddle(int[] array){
		Scanner scanner = new Scanner(System.in);
		double arrayShare;
		System.out.println("Enter Array ");
		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt(); 
			arraySum = arraySum + array[i];
		}
		arrayShare = arraySum / array.length;
		System.out.println(arrayShare);
		System.out.println("Exit three Arrays");
		return arrayShare;
	}
	public static String MinReverseMax (int [] array){
		Scanner scanner = new Scanner(System.in);
		int min = Integer.MAX_VALUE;
		int max = array[0];
		int tempMin = 0;
		int tempMax = 0;
		int temp;
		System.out.println("Enter Array ");
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
			for (int j = i; j < array.length; j++){
				if (array[i] < min){
					min = array[i];
					tempMin = i;
				}
				if (array[j] > max){
					max = array[j];
					tempMax = j;
				}
			}
		}
		System.out.println(Arrays.toString(array));
		temp = array[tempMin];
		array[tempMin] = array[tempMax];
		array[tempMax] = temp;
		System.out.println("Min = " + min + " : " + "Max =" + max);
		System.out.println("Exit four array");
		return Arrays.toString(array);
	}
	public static String BubbleArray(int[] array, int size){
		Scanner scanner = new Scanner(System.in);
		int a, b, c;
		System.out.println("Enter Array ");
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		for (a = 1; a < size; a++)
			for (b = size - 1; b >= a; b--) {
				if (array[b-1] > array[b]){
					c = array[b - 1];
					array[b - 1] = array[b];
					array[b] = c;
				}
			}
		System.out.print("Sorting Array ");
		for (int i = 0; i < size; i++){
			System.out.print(" ");
		}
		return Arrays.toString(array);
	}
	public static int NumbersArray(){
		int []array = {4, 8, 6, 1, 3};
		int result = 0;
		int number = 1;
		for (int j = array.length - 1; j >= 0; j--){
			result = result + array[j] * number;
			number *= 10;
		}
		System.out.println("Exit");
		return result;
	}
}