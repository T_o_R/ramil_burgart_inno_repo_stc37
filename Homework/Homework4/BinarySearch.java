package WorkFour;

import java.util.Scanner;

public class BinarySearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberForSearch = scanner.nextInt();
        int [] arrays = {13, 15, 16, 32, 45, 63, 74, 82, 85, 91, 100};
        System.out.println(binarySearch(arrays, numberForSearch));
    }
    public static int binarySearch(int [] array, int element) {
        int left = 0;
        int right = array.length - 1;
        return binarySearch(array, element, right, left);
    }
    public static int binarySearch(int [] array, int element, int right, int left){
        int middle= left + (right - left) / 2;
        System.out.println(left + " " + right);
        if(left > right){
            return -1;
        }
        if (array[middle] < element) {
            return binarySearch(array, element, right, middle + 1);
        }else if (array[middle] > element) {
            return binarySearch(array, element, middle - 1, left);
        }else if (array[middle] == element) {
            System.out.println(element);
            return element;
        }
        return element;
    }
}
