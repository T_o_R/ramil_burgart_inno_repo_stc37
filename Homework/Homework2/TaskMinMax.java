package Homework2;

import java.util.Scanner;
import java.util.Arrays;

class TaskMinMax {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		int[] array = new int[size];
		int min = Integer.MAX_VALUE;
		int max = array[0];
		int tempMin = 0;
		int tempMax = 0;
		int temp;
		System.out.println("Enter Array ");
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
			for (int j = i; j < array.length; j++){	
				if (array[i] < min){
					min = array[i];
					tempMin = i;
				}
				if (array[j] > max){
					max = array[j];
					tempMax = j;
				}				
			}
		}
		System.out.println(Arrays.toString(array));
		temp = array[tempMin];
		array[tempMin] = array[tempMax];
		array[tempMax] = temp;	
		System.out.println("Min = " + min + " : " + "Max =" + max);
		System.out.println(Arrays.toString(array));
		
	}
}