package Homework2;

import java.util.Scanner;

class BubbleSorting{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		int array[] = new int[size];
		int a, b, c;
		System.out.println("Enter Array ");
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
			System.out.println();
		}		
		for (a = 1; a < size; a++)
			for (b = size - 1; b >= a; b--) {
				if (array[b-1] > array[b]){
					c = array[b - 1];
					array[b - 1] = array[b];
					array[b] = c;
				}
			}
			System.out.print("Sorting Array ");
			for (int i = 0; i < size; i++){
				System.out.print(" " + array[i]);
				System.out.println();
			}
	}
}