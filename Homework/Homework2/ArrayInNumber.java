package Homework2;

import java.util.Scanner;

class ArrayInNumber {
	public static void main(String[] args) {
		int array [] = {4, 8, 6, 1, 3};
		int result = 0;
		int number = 1;
		for (int j = array.length - 1; j >= 0; j--){
			result = result + array[j] * number;
			number *= 10;
		}
		System.out.println(result);
	}
}