package Homework2;

import java.util.Scanner;

class ReverseArray{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		int array[] = new int [size];
		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}
		for (int j = array.length - 1; j >= 0; j--){
			System.out.print(array[j] + " ");
		}
	}
}